#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" Gitlab Webhook Receiver: common code"""

import logging
import os


class SignalFunctions:
    def __init__(self, config):
        try:
            self.state_dir_name = config["common"]["state_dir"]
        except KeyError as err:
            logging.error("unable to parse state directory configuration: %s" % err)
            raise err

        self.check_signal_dir()

    def check_signal_dir(self):
        check_msg = None

        if not os.path.exists(self.state_dir_name):
            check_msg = "%s does not exists" % self.state_dir_name
        if not os.path.isdir(self.state_dir_name):
            check_msg = "%s is not a directory" % self.state_dir_name
        if not os.access(self.state_dir_name, os.W_OK):
            check_msg = "%s is not writable" % self.state_dir_name

        if check_msg is not None:
            logging.error(check_msg)
            raise ValueError(check_msg)

    def _escape_project_url(self, project):
        """... so that it is usable as part of file names"""
        return project.replace(os.sep, "_")

    def _get_signal_file_name(self, project):
        return os.path.join(
            self.state_dir_name, self._escape_project_url(project) + ".signal"
        )


class ReceiverSignalFunctions(SignalFunctions):
    def create_signal_file(self, project, json_payload):
        with open(self._get_signal_file_name(project), "wb") as sf:
            sf.write(json_payload)


class CronjobSignalFunctions(SignalFunctions):
    def __init__(self, config):
        super().__init__(config)

        self.projects = []
        for key in config:
            if key == "common":
                continue
            self.projects.append(key)

    def get_projects(self):
        """
        @return list of projects for which a signal file exists
        """
        result = []
        for project in self.projects:
            if os.path.exists(self._get_signal_file_name(project)):
                result.append(project)
        return result

    def get_json_payload(self, project):
        result = None
        with open(self._get_signal_file_name(project), "rb") as sf:
            result = sf.read()
        return result

    def remove_signal_file(self, project):
        sfn = self._get_signal_file_name(project)
        try:
            os.remove(sfn)
        except FileNotFoundError as err:
            logging.error("unable to delete %s: %s" % (sfn, err))

    def _get_lock_file_name(self, project):
        return os.path.join(
            self.state_dir_name, self._escape_project_url(project) + ".lock"
        )

    def lock(self, project):
        """raises OSError if unable to acquire lock"""
        lfn = self._get_lock_file_name(project)
        if os.path.exists(lfn):
            raise OSError("lock %s already exists" % lfn)
        with open(lfn, "w") as lf:
            lf.write("%d" % os.getpid())

    def unlock(self, project):
        lfn = self._get_lock_file_name(project)
        if not os.path.exists(lfn):
            logging.warning("lock %s does not exist" % lfn)
        os.remove(lfn)
