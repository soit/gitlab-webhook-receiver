# gitlab-webhook-receiver
Less simple gitlab webhook receiver. The main goal is to run the commands as user other than web server user. Secondary goal is to better handle the cases when commands can take long time to finish, e.g. better handling of hook received while commands triggered by previous hook are still running.

## Source
The idea and base of the script is from [docker-hook](https://github.com/schickling/docker-hook),
forked to [pstauffer/gitlab-webhook-receiver](https://github.com/pstauffer/gitlab-webhook-receiver),
then to [crisnao2/gitlab-webhook-receiver](https://github.com/crisnao2/gitlab-webhook-receiver)
and finally [to this OEG repository](https://gitlab.com/oeglobal/gitlab-webhook-receiver).

Other information for developer is bellow in [Development section](#development).

## Configuration

### Gitlab Secret Token
The script requires, that the gitlab secret token is set! You can define the value in the [configuration file](#example-config).

### Gitlab Project Homepage
The structure of the [configuration file](#example-config) requires the homepage of the gitlab project as key.

### Command
Define, which command should be run after the hook was received.

### Command by Event
Define, which command should be run after the hook was received with base in event setted.

### Example config
```
# file: config.yaml
---
common:
  state_dir: /run/gitlab-webhook-receiver
# myrepo
https://git.example.ch/exmaple/myrepo:
  command: uname
  gitlab_token: mysecret-myrepo
# test-repo
https://git.example.ch/exmaple/test-repo:
  command: uname
  gitlab_token: mysecret-test-repo
# test-repo2 with command by events
https://git.example.ch/example/test-repo2:
  commands:
    events:
      issue:
        command: [echo, "event issue received"]
      note:
        command: [echo, "event note received"]
  gitlab_token: mysecret-test-repo
```

### Example systemd config
See:
- [gitlab-webhook-receiver.service](examples/gitlab-webhook-receiver.service)
- [gitlab-webhook-cronjob.service](examples/gitlab-webhook-cronjob.service)
- [gitlab-webhook-cronjob.timer](examples/gitlab-webhook-cronjob.timer)


## Script Arguments

### Port
Define the listen port for the webserver. Default: **8666**

### Addr
Define the listen address for the webserver. Default: **0.0.0.0**

### Cfg
Define the path to your configuration file. Default: **config.yaml**



## Run Webhook Script

```
python gitlab-webhook-receiver.py --port 8080 --cfg /etc/hook.yaml
```


### Help
```
usage: gitlab-webhook-receiver.py [-h] [--addr ADDR] [--port PORT] [--cfg CFG]

Gitlab Webhook Receiver

optional arguments:
  -h, --help   show this help message and exit
  --addr ADDR  address where it listens (default: 0.0.0.0)
  --port PORT  port where it listens (default: 8666)
  --cfg CFG    path to the config file (default: config.yaml)
  --log stream || path to file    Type of the log, stream or path to file
```


## Run Cronjob Script

```
python gitlab-webhook-cronjob.py --cfg /etc/hook.yaml
```


### Help
```
usage: gitlab-webhook-cronjob.py [-h] [--cfg CFG]

Gitlab Webhook Cronjob

optional arguments:
  -h, --help   show this help message and exit
  --cfg CFG    path to the config file (default: config.yaml)
  --log stream || path to file    Type of the log, stream or path to file
```


## Development

Branches, pull-requests, releases, etc.: according to [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/) but ...

... in order to allow better overview for current and future contributors, we'd like to conclude features with pull-requests,
i.e. instead of `git flow feature finish MYFEATURE` do following:

1. `git flow feature publish MYFEATURE`
2. go to GitLab and open pull-request from your feature branch to `develop`
3. review + adjustments
4. merge + delete branch after merge

### Git hooks

For source code formatting, etc.:

`pre-commit install`

### Triggering Webhook Script locally

For example:

```
curl --json '{"project": {"web_url": "https://git.example.ch/exmaple/test-repo"}}' \
  -H "X-Gitlab-Token: mysecret-test-repo" \
  http://localhost:8666/
```
