#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" Gitlab Webhook Receiver """
# Based on: https://github.com/schickling/docker-hook

import json
import yaml
from subprocess import Popen, PIPE, STDOUT
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
from importlib import import_module

try:
    # For Python 3.0 and later
    from http.server import HTTPServer
    from http.server import BaseHTTPRequestHandler
except ImportError:
    # Fall back to Python 2
    from BaseHTTPServer import BaseHTTPRequestHandler
    from BaseHTTPServer import HTTPServer as HTTPServer
import sys
import logging
from datetime import datetime

from gitlab_webhook_common import ReceiverSignalFunctions


class RequestHandler(BaseHTTPRequestHandler):
    """A POST request handler."""

    # Attributes (only if a config YAML is used)
    # command, gitlab_token, foreground
    def get_info_from_config(self, project, config):
        # get token from config file
        self.gitlab_token = config[project]["gitlab_token"]
        logging.info("Load project '%s'", project)

    def do_token_mgmt(
        self, project_url, gitlab_token_header, json_payload, json_params
    ):
        # Check if the gitlab token is valid
        if gitlab_token_header == self.gitlab_token:

            try:
                signal_functions.create_signal_file(project_url, json_payload)
                self.send_response(200, "OK")
            except Exception as err:
                self.send_response(500, "Error")
                logging.error("Failed to handle the hook.")
                logging.error(err)
        else:
            logging.error("Not authorized, Gitlab_Token not authorized")
            self.send_response(401, "Gitlab Token not authorized")

    def process_from_module(self, gitlab_token_header, json_params):
        for m in modules:
            logging.info("Running main() from module '%s'", m)
            m.main(gitlab_token_header, json_params, self, args)

    def do_POST(self):
        logging.info("Hook received")

        if sys.version_info >= (3, 0):
            # get payload
            header_length = int(self.headers["Content-Length"])
            # get gitlab secret token
            gitlab_token_header = self.headers["X-Gitlab-Token"]
        else:
            header_length = int(self.headers.getheader("content-length", "0"))
            gitlab_token_header = self.headers.getheader("X-Gitlab-Token")

        json_payload = self.rfile.read(header_length)
        json_params = {}
        if len(json_payload) > 0:
            json_params = json.loads(json_payload.decode("utf-8"))

        if "project" in json_params and "web_url" in json_params["project"]:
            project_url = json_params["project"]["web_url"]
        elif "repository" in json_params and "homepage" in json_params["repository"]:
            project_url = json_params["repository"]["homepage"]
        else:
            self.send_response(500, "KeyError")
            logging.error("No project provided by the JSON payload")
            self.end_headers()
            return

        if args.modules:
            self.process_from_module(gitlab_token_header, json_params)
            self.end_headers()
            return

        try:
            self.get_info_from_config(project_url, config)
            self.do_token_mgmt(
                project_url, gitlab_token_header, json_payload, json_params
            )
        except KeyError as err:
            self.send_response(500, "KeyError")
            if err == project_url:
                logging.error(
                    "project_url '%s' not found in %s", project_url, args.cfg.name
                )
            elif err == "command":
                logging.error("Key 'command' not found in %s", args.cfg.name)
            elif err == "gitlab_token":
                logging.error("Key 'gitlab_token' not found in %s", args.cfg.name)
        finally:
            self.end_headers()


def get_parser():
    """Get a command line parser."""
    parser = ArgumentParser(
        description=__doc__, formatter_class=ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "--addr", dest="addr", default="0.0.0.0", help="address where it listens"
    )
    parser.add_argument(
        "--port",
        dest="port",
        type=int,
        default=8666,
        metavar="PORT",
        help="port where it listens",
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--cfg", dest="cfg", type=FileType("r"), help="path to the config file"
    )
    group.add_argument(
        "-m",
        "--module",
        action="append",
        dest="modules",
        help="path to a python module to run",
    )
    parser.add_argument(
        "--log",
        dest="log",
        default="stream",
        help="Type of the log, stream or path to file",
    )
    return parser


def main(addr, port):
    """Start a HTTPServer which waits for requests."""
    httpd = HTTPServer((addr, port), RequestHandler)
    httpd.serve_forever()


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()

    if args.log == "stream":
        logging.basicConfig(
            format="%(asctime)s %(levelname)s %(message)s",
            level=logging.DEBUG,
            stream=sys.stdout,
        )
    else:
        logging.basicConfig(
            format="%(asctime)s %(levelname)s %(message)s",
            level=logging.DEBUG,
            filename=args.log,
        )

    if args.cfg:
        config = yaml.safe_load(args.cfg)
    elif args.modules:
        modules = [import_module(m, package=".") for m in args.modules]

    signal_functions = ReceiverSignalFunctions(config)

    main(args.addr, args.port)
