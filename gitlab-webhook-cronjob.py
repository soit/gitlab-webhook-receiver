#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" Gitlab Webhook Receiver """
# Based on: https://github.com/schickling/docker-hook

import json
import yaml
from subprocess import Popen, PIPE, STDOUT
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
from importlib import import_module
import sys
import logging

from gitlab_webhook_common import CronjobSignalFunctions


class CronjobHandler:
    """Cronjob handler."""

    # Attributes (only if a config YAML is used)
    # command, gitlab_token, foreground
    def get_info_from_config(self, project, config):
        # get command and token from config file
        self.command = (
            config[project]["command"] if "command" in config[project] else False
        )
        self.commands = (
            config[project]["commands"] if "commands" in config[project] else False
        )
        self.foreground = (
            not config[project]["background"]
            if "background" in config[project]
            else True
        )
        logging.info("Load project '%s'", project)

    # TODO: since this no longer "manages token" but "runs commands" rename it to run_commands
    def do_token_mgmt(self, project):
        try:
            signal_functions.lock(project)
        except OSError as err:
            logging.error("unable to acquire lock for %s" % project)
            return

        try:
            json_payload = signal_functions.get_json_payload(project)

            if self.command:
                logging.info("Start executing '%s'" % self.command)
                # run command in background
                p = Popen(self.command, stdin=PIPE)
                p.stdin.write(json_payload)
                p.stdin.flush()

                if self.foreground:
                    p.communicate()

                p.stdin.close()
            if self.commands:
                if "events" in self.commands and (
                    "object_kind" in json_params
                    and json_params["object_kind"] in self.commands["events"]
                ):
                    event = self.commands["events"][json_params["object_kind"]]
                    p_event_foreground = (
                        "background" in event and not event["background"]
                    )

                    logging.info(
                        "Start executing command '%s' to event '%s'",
                        event["command"],
                        json_params["object_kind"],
                    )

                    p_event = Popen(event["command"], stdin=PIPE)
                    p_event.stdin.write(json_payload)
                    p_event.stdin.flush()

                    if p_event_foreground:
                        p_event.communicate()

                    p_event.stdin.close()
            logging.debug("Processing %s: OK" % project)
        except OSError as err:
            logging.debug("Processing %s: error" % project)
            logging.error("Command could not run successfully.")
            logging.error(err)
        finally:
            signal_functions.remove_signal_file(project)
            signal_functions.unlock(project)

    def process_from_module(self, project):
        for m in modules:
            logging.info("Running main() from module '%s'", m)
            m.main(gitlab_token_header, json_params, self, args)

    def run(self):
        logging.debug("Cronjob started")

        for project in signal_functions.get_projects():
            logging.debug("Processing %s" % project)
            if args.modules:
                self.process_from_module(project)
                return

            try:
                self.get_info_from_config(project, config)
                self.do_token_mgmt(project)
            except KeyError as err:
                self.send_response(500, "KeyError")
                if err == project_url:
                    logging.error(
                        "project_url '%s' not found in %s", project_url, args.cfg.name
                    )
                elif err == "command":
                    logging.error("Key 'command' not found in %s", args.cfg.name)
                elif err == "gitlab_token":
                    logging.error("Key 'gitlab_token' not found in %s", args.cfg.name)


def get_parser():
    """Get a command line parser."""
    parser = ArgumentParser(
        description=__doc__, formatter_class=ArgumentDefaultsHelpFormatter
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--cfg", dest="cfg", type=FileType("r"), help="path to the config file"
    )
    group.add_argument(
        "-m",
        "--module",
        action="append",
        dest="modules",
        help="path to a python module to run",
    )
    parser.add_argument(
        "--log",
        dest="log",
        default="stream",
        help="Type of the log, stream or path to file",
    )
    return parser


def main():
    """GitLab Webhook cronjob."""
    cronjob_handler = CronjobHandler()
    cronjob_handler.run()


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()

    if args.log == "stream":
        logging.basicConfig(
            format="%(asctime)s %(levelname)s %(message)s",
            level=logging.DEBUG,
            stream=sys.stdout,
        )
    else:
        logging.basicConfig(
            format="%(asctime)s %(levelname)s %(message)s",
            level=logging.DEBUG,
            filename=args.log,
        )

    if args.cfg:
        config = yaml.safe_load(args.cfg)
    elif args.modules:
        modules = [import_module(m, package=".") for m in args.modules]

    signal_functions = CronjobSignalFunctions(config)

    main()
